import Pie from "./Pie";

function App() {
  return (
    <div className="App">
      <Pie />
    </div>
  );
}

export default App;
